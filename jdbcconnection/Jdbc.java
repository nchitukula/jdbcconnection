package jdbcconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class for implementing the various methods of SQL from JDBC.
 * 
 * @author nchitukula
 */
public class Jdbc {

	static final String DB_URL = "jdbc:mysql://localhost/student";
	static final String USER = "root";
	static final String PASS = "root";

	/**
	 * Helper method for creating SQLTable
	 * 
	 * @return
	 */
	public boolean DDLCreateTable(String tablename) {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			String sql = "CREATE TABLE neha " + 
				"(id INT, " + 
					" Name VARCHAR(255))";
			stmt.executeUpdate(sql);
			System.out.println("Created table in given database...");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Helper method for Inserting data into table.
	 * 
	 * @return
	 */
	public boolean DMLInsertData() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			System.out.println("Inserting records into the table...");
			String sql = "INSERT INTO neha VALUES (100, 'vanga')";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO neha VALUES(101,'nehaa')";
			stmt.execute(sql);
			sql = "INSERT INTO neha VALUES(102,'anjali')";
			stmt.execute(sql);
			sql = "INSERT INTO neha VALUES(103,'priya')";
			stmt.execute(sql);
			return true; // succesfully inserting values.
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;// fails to insert
	}

	/**
	 * Helper method for getting data by id from table.
	 * 
	 * @param id
	 * 
	 * @return
	 */
	public boolean DDLGetdatabyId(int id) {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {

			String sql = "Select *from neha where + id =+id";
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				// System.out.println(rs.getString());//coloumn 1
				System.out.println("Selected data by id:" + id);
			}
			return true;// getting id succesfully/
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false; // fails to insert.
	}

	/**
	 * Helper method for Deleting data by id.
	 * 
	 * @return
	 */
	public boolean DDLDeletedatabyId() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {

			String sql = "delete from neha where id = 101";
			stmt.executeUpdate(sql);
			System.out.println("deleted data by id....");
			return true;// deleted id succesfully/
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false; // fails to delete.
	}

	/**
	 * Helper method for Counting the data/Records from the table.
	 * 
	 * @return
	 */
	public boolean DataCount() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {

			String sql = "Select Count(*) from neha";
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int count = rs.getInt(1);

			System.out.println("Data count in table: " + count);
			return true;// counted the data succesfully/
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false; // fails to count.
	}

	/**
	 * Helper method for Updating the data by id.
	 * 
	 * @return
	 */
	public boolean UpdatedatabyId() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {

			String sql = "Update neha Set Name = 'priya' where id=103";
			stmt.executeUpdate(sql);
			System.out.println("Update data by Id in table...");
			return true;// updated the data succesfully/
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false; // fails to update.
	}

	/**
	 * Helper method for deleting the whole table.
	 * 
	 * @return
	 */
	public boolean DDLDropTable() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			String sql = "Drop table neha";
			stmt.executeUpdate(sql);
			System.out.println("Deleting the table");
			return true;// updated the data succesfully/
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false; // fails to update.
	}
}
