package jdbcconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class for implementing the various methods of SQL from JDBC.
 * 
 * @author nchitukula
 */
public class Jdbc {

	static final String DB_URL = "jdbc:mysql://localhost/student";
	static final String USER = "root";
	static final String PASS = "root";

	/**
	 * Helper method for creating SQLTable
	 * 
	 * @return
	 */
	public boolean DDLCreateTable(String tablename) {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			String sql = "CREATE TABLE neha " 
				+ "(id INT, " + 
					" Name VARCHAR(255))";
			stmt.executeUpdate(sql);
			System.out.println("Created table in given database...");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Helper method for Inserting data into table.
	 * 
	 * @return
	 */
	public boolean DMLInsertData() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			System.out.println("Inserting records into the table...");
			String sql = "INSERT INTO neha VALUES (100, 'vanga')";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO neha VALUES(101,'nehaa')";
			stmt.execute(sql);
			sql = "INSERT INTO neha VALUES(102,'anjali')";
			stmt.execute(sql);
			sql = "INSERT INTO neha VALUES(103,'priya')";
			stmt.execute(sql);
			return true; // succesfully inserting values.
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;// fails to insert
	}

	/**
	 * Helper method for getting data by id from table.
	 * 
	 * @return
	 */
	public boolean DDLGetdatabyId() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			System.out.println("Selected data by id....");
			String sql = "Select *from neha where" + 
			"(id = +id)";

			stmt.executeUpdate(sql);
			return true;// getting id succesfully/
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false; // fails to insert.
	}
}
