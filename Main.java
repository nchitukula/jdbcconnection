package jdbcconnection;
/**
 * Class for implementing the Main method.
 * 
 * @author nchitukula
 *
 */
public class Main {
	
	public static void main (String[] args) {
		Jdbc jd = new Jdbc();
		jd.DDLCreateTable("neha");
		jd.DMLInsertData();
		jd.DDLGetdatabyId();
	}

}
